package com.devskiller.service;

import com.devskiller.dao.ItemRepository;
import com.devskiller.model.Item;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepository;

	public ItemService(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	/**
	 * Get items's title with average rating lower than input rating
	 * 
	 * @param rating a Double of request rating
	 * @return a List of item's title
	 */
	public List<String> getTitlesWithAverageRatingLowerThan(Double rating) {
		List<Item> resultList = itemRepository.findItemsWithAverageRatingLowerThan(rating);
		return resultList.stream().map(Item::getTitle).collect(Collectors.toList());
	}

}
