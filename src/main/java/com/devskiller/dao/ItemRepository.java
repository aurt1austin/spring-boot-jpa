package com.devskiller.dao;

import com.devskiller.model.Item;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Long> {

	@Query(value = "select i from Item i left join i.reviews r group by i having avg(coalesce(r.rating, 0)) < :rating")
    List<Item> findItemsWithAverageRatingLowerThan(Double rating);
}
