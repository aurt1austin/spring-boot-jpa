CREATE TABLE IF NOT EXISTS ticket (
	id int4 NOT NULL,
	"name" varchar NOT NULL,
	user_id int4 NOT NULL
);

INSERT INTO public.ticket
(id, "name", user_id)
VALUES(1, 'Name 1', 9094);

INSERT INTO public.ticket
(id, "name", user_id)
VALUES(2, 'Name 2', 9094);

INSERT INTO public.ticket
(id, "name", user_id)
VALUES(3, 'Name 3', 9094);

INSERT INTO public.ticket
(id, "name", user_id)
VALUES(4, 'Name 4', 9094);

INSERT INTO public.ticket
(id, "name", user_id)
VALUES(5, 'Name 5', 9094);